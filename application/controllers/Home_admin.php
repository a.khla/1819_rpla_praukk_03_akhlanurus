<?php

class Home_admin extends CI_Controller
{
    public function index()
    {
        // tambahan load merupakan sebuah fungsi yang sering dipakai untuk meng-load macam-macam
        // saya mau cari sebuah file yang namanya home.php yang ada di folder views
        $data['judul'] = 'Halaman Home';
        // $data['nama'] = $nama;
        $this->load->view('home_admin/index', $data);
    }
}
