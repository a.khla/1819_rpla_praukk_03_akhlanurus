<link href="<?= base_url('assets/'); ?>dist/css/bootstrap.min.css" rel="stylesheet">
<div>
    <div class="text-left" style="height: 50px; background-color: #FFFFFF">
        <img src="<?= base_url('assets/'); ?>images/logo.png" style="width: 120px; margin-top: -35px; margin-left: 100px">
    </div>
    <div style="background-color: #5CB9D6; height: 500px" class="d-flex flex-row bd-highlight shadow bg-body rounded">
        <div class="rounded float-start" style="padding-right: 300px; width: 100%">
            <div style="margin-left: 100px; margin-top: 100px; width: 100%">
                <text style="color: #FFFFFF; font-size: 35pt; font-weight: bold">
                    FIND YOUR
                </text>
                <br>
                <text style="color: #FFFFFF; font-size: 18pt">
                    FACILITIES AND INFRASTRUCTURE
                </text>
                <button style="margin-top: 50px; width: 240px; height: 50px; background-color: #0B7698" type="submit" class="btn rounded-pill">
                    <text style="font-weight: bold; color: #FFFFFF">LOGIN</text>
                </button>
            </div>
        </div>
        <div class="rounded float-end">
            <img src="<?= base_url('assets/'); ?>images/classroom.jpg" style="height: 100%; width: 100%">
        </div>
    </div>
    <div style="background-color: #FFFFFF; height: 1300px; width: 100%;">
        <div class="container">
            <div class="flex-row d-flex">
                <div class="shadow p-3 mb-5 bg-body rounded" style="background-color: #5CB9D6; width: 400px; height: 500px; margin-top: 120px">
                    <div>
                        <img src="<?= base_url('assets/'); ?>images/1.jpeg" style="width: 400px; height: 500px; margin-left: 62px; margin-top: 53px;" class="rounded float-start">
                    </div>
                </div>
                <div style="width:100%; margin-top:120px; margin-left:230px;">
                    <text style="color: #111111; font-size: 35pt; font-weight: bold">
                        FIND YOUR
                    </text>
                    <br>
                    <text style="color: #111111; font-size: 18pt">
                        FACILITIES AND INFRASTRUCTURE
                    </text>
                    <br>
                    <text style="color: #111111; font-size: 18pt">
                        FACILITIES AND INFRASTRUCTURE
                    </text>
                    <br>
                    <text style="color: #111111; font-size: 18pt">
                        FACILITIES AND INFRASTRUCTURE
                    </text>
                </div>
            </div>
            <div class="container" style="margin-left:-70px; margin-top:-200px">
                <div class="flex-row d-flex">
                    <div style="width:100%; margin-top:350px; margin-left:120px;">
                        <text style="color: #111111; font-size: 35pt; font-weight: bold">
                            FIND YOUR
                        </text>
                        <br>
                        <text style="color: #111111; font-size: 18pt">
                            FACILITIES AND INFRASTRUCTURE
                        </text>
                        <br>
                        <text style="color: #111111; font-size: 18pt">
                            FACILITIES AND INFRASTRUCTURE
                        </text>
                        <br>
                        <text style="color: #111111; font-size: 18pt">
                            FACILITIES AND INFRASTRUCTURE
                        </text>
                    </div>
                    <div class="rounded float-end d-flex flex-row shadow p-3 mb-5 bg-body rounded" style="float:right; background-color: #5CB9D6; width: 400px; height: 500px; margin-left:150px;">
                        <div>
                            <img src="<?= base_url('assets/'); ?>images/smkn1-3.png" style="width: 400px; height: 500px; margin-left: 62px; margin-top: 53px;" class="rounded float-start">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="width: 100%; background-color: #111111">
        <img src="<?= base_url('assets/'); ?>images/books-library.jpg" style="height: 350px; width: 100%">
    </div>
    <div style="background-color: #5CB9D6; width: 100%; height: 421px;">
        <div class="container">
            <div class="d-flex flex-row bd-highlight" style="margin-left:400px; padding-top:200px">
                <a href="">
                    <h6 style="color:#FFFFFF; text-align:center; font-family:verdana">Privacy Policy</h6>
                </a>
                <a href="">
                    <h6 style="color:#FFFFFF; text-align:center; font-family:verdana; margin-left: 10px">Terms of Use</h6>
                </a>
                <a href="">
                    <h6 style="color:#FFFFFF; text-align:center; font-family:verdana; margin-left: 10px">Contact Support</h6>
                </a>
            </div>
            <div class="d-flex flex-row bd-highlight" style="margin-left:400px;">
                <a href="">
                    <h6 style="color:#FFFFFF; text-align:center; font-family:verdana">Copyright 2020</h6>
                </a>
                <a href="">
                    <h6 style="color:#FFFFFF; text-align:center; font-family:verdana; margin-left: 10px">Inventone.</h6>
                </a>
                <a href="">
                    <h6 style="color:#FFFFFF; text-align:center; font-family:verdana; margin-left: 10px">All Right Reserved</h6>
                </a>
            </div>
            <div></div>
            <div class="d-flex flex-row bd-highlight" style="margin-top:15px; margin-left:520px">
                <a href="">
                    <img src="<?= base_url('assets/'); ?>images/twitter-white.png" style="width: 40px ;">
                </a>
                <a href="">
                    <img src="<?= base_url('assets/'); ?>images/instagram-white.png" style="width: 40px; margin-left: 15px ;">
                </a>
                <a href="">
                    <img src="<?= base_url('assets/'); ?>images/g+white.png" style="width: 40px; margin-left: 15px ;">
                </a>
            </div>
        </div>
    </div>
</div>