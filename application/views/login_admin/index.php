<link href="<?= base_url('assets/'); ?>dist/css/bootstrap.min.css" rel="stylesheet">
<div>
    <div style="background: #FFFFFF; height: 1069px">
        <div>
            <div class="text-center" style="margin-top: -20;">
                <img src="<?= base_url('assets/'); ?>images/logo.png">
            </div>
            <div class="d-flex flex-row bd-highlight" style="margin-top: -100px">
                <div class="rounded float-start shadow p-3 mb-5 bg-body rounded" style="background-color: #5CB9D6; width: 400px; height: 500px; margin-top: 123px; margin-left: 133px;">
                    <img src="<?= base_url('assets/'); ?>images/1.jpeg" style="width: 400px; height: 500px; margin-left: 62px; margin-top: 53px;" class="rounded float-start">
                </div>
                <div class="rounded float-end shadow p-3 mb-5 bg-body rounded" style="background-color: #FFFFFF; width: 400px; height: 500px; margin-top: 123px; margin-left: 242px;">
                    <form>
                        <label style="margin-top: 50px; margin-left: 15px" for="login">
                            <h4>Login</h4>
                        </label>
                        <div style="margin-top: 30px; margin-left: 15px; margin-right:15px; margin-bottom: 40px">
                            <input type="text" class="form-control" id="nama_pengguna" placeholder="Nama Pengguna atau Email">
                        </div>
                        <div style="margin-top: 10px; margin-left: 15px; margin-right:15px; margin-bottom: 50px">
                            <input type="password" class="form-control" id="kata_sandi" placeholder="Kata Sandi">
                        </div>
                        <button style="margin-top: 10px; margin-left: 15px; margin-bottom: 20px; width: 340px; height: 50px" type="submit" class="btn btn-primary rounded-pill">Login</button>
                    </form>
                </div>
            </div>
        </div>
        <div style="background-color: #5CB9D6; width: 100%; height: 421px; margin-top: 150px">
            <div class="container">
                <div class="d-flex flex-row bd-highlight" style="margin-left:400px; padding-top:200px">
                    <a href="">
                        <h6 style="color:#FFFFFF; text-align:center; font-family:verdana">Privacy Policy</h6>
                    </a>
                    <a href="">
                        <h6 style="color:#FFFFFF; text-align:center; font-family:verdana; margin-left: 10px">Terms of Use</h6>
                    </a>
                    <a href="">
                        <h6 style="color:#FFFFFF; text-align:center; font-family:verdana; margin-left: 10px">Contact Support</h6>
                    </a>
                </div>
                <div class="d-flex flex-row bd-highlight" style="margin-left:400px;">
                    <a href="">
                        <h6 style="color:#FFFFFF; text-align:center; font-family:verdana">Copyright 2020</h6>
                    </a>
                    <a href="">
                        <h6 style="color:#FFFFFF; text-align:center; font-family:verdana; margin-left: 10px">Inventone.</h6>
                    </a>
                    <a href="">
                        <h6 style="color:#FFFFFF; text-align:center; font-family:verdana; margin-left: 10px">All Right Reserved</h6>
                    </a>
                </div>
                <div></div>
                <div class="d-flex flex-row bd-highlight" style="margin-top:15px; margin-left:520px">
                    <a href="">
                        <img src="<?= base_url('assets/'); ?>images/twitter-white.png" style="width: 40px ;">
                    </a>
                    <a href="">
                        <img src="<?= base_url('assets/'); ?>images/instagram-white.png" style="width: 40px; margin-left: 15px ;">
                    </a>
                    <a href="">
                        <img src="<?= base_url('assets/'); ?>images/g+white.png" style="width: 40px; margin-left: 15px ;">
                    </a>
                </div>
            </div>
        </div>
    </div>